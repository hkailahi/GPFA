# guitar

## Research

- https://www.reddit.com/r/musictheory/comments/1jd894/looking_for_an_algorithm_that_generates_chord/
- http://web.mit.edu/music21/
- https://github.com/cuthbertlab/music21
  - https://github.com/cuthbertLab/music21/blob/master/music21/chord/tables.py
- https://github.com/adamstark/Chord-Detector-and-Chromagram
- https://github.com/AnthonyReid99/PyChordFinder/blob/master/ChordFinder.py
